package Database;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.sql.rowset.serial.SerialBlob;

import com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait;

import utils.Constants;

public class DatabaseAccessObject {
	
	@SuppressWarnings("finally")
	public static Connection getConnection(){
		Connection con = null;
		try{  
			Class.forName(Constants.JDBC_DRIVER);  
			con=DriverManager.getConnection(Constants.JDBC_DB_URL, Constants.JDBC_USER, Constants.JDBC_PASS);  
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			return con;
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean isUserIdPresent(String userId){
		boolean status = false;
		Connection con = null;
		try{
			con = getConnection();
			
			PreparedStatement ps = con.prepareStatement(Constants.IS_USER_ID_PRESENT);
			ps.setString(1, userId);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				if(userId.equals(rs.getString(1)))
					status = true;
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
			con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	public static byte[] toByteArray(Object obj) throws IOException {
        byte[] bytes = null;
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(obj);
            oos.flush();
            bytes = bos.toByteArray();
        } finally {
            if (oos != null) {
                oos.close();
            }
            if (bos != null) {
                bos.close();
            }
        }
        return bytes;
    }
	
	public static Object blobToObject(byte[] bytes) throws IOException, ClassNotFoundException {
        Object obj = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bis = new ByteArrayInputStream(bytes);
            ois = new ObjectInputStream(bis);
            obj = ois.readObject();
        } finally {
            if (bis != null) {
                bis.close();
            }
            if (ois != null) {
                ois.close();
            }
        }
        return obj;
    }
	
	@SuppressWarnings("finally")
	public static Blob objectToBlob(HashMap<String, Double> obj){
		Blob blob = null;
		try {
			blob = new SerialBlob(toByteArray(obj));
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}finally{
			return blob;
		}
	}

	
	@SuppressWarnings("finally")
	public static boolean insertUserPersonality(String userId, String userName, String date, Profile profile) {
		boolean status = false;
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.INSERT_USER_PERSONALITY);
			ps.setString(1, userId);
			ps.setString(2, userName);
			ps.setString(3, date);
			if(ps.executeUpdate() > 0){
				System.out.println("user_Personality Added");
				if(insertPersonality(userId, profile) && insertValues(userId, profile) && insertNeeds(userId, profile))
					status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	@SuppressWarnings({ "finally", "null" })
	public static boolean insertPersonality(String userId, Profile profile){
		boolean status = false;
		HashMap<String, Double> openness = new HashMap<>();
		HashMap<String, Double> conscientiousness = new HashMap<>();
		HashMap<String, Double> extraversion = new HashMap<>();
		HashMap<String, Double> agreeableness = new HashMap<>();
		HashMap<String, Double> emotional_range = new HashMap<>();
		
		List<Trait> personality = profile.getPersonality();
		
		for(Trait trait : personality){
			List<Trait> personalityChild = trait.getChildren();
			if(trait.getTraitId().equals("big5_openness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					openness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_conscientiousness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					conscientiousness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_extraversion")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					extraversion.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_agreeableness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					agreeableness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_neuroticism")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					emotional_range.put(traitChild.getName(), percent);
				}
			}
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.INSERT_PERSONALITY);
			ps.setString(1, userId);
			ps.setBlob(2, objectToBlob(openness));
			ps.setBlob(3, objectToBlob(conscientiousness));
			ps.setBlob(4, objectToBlob(extraversion));
			ps.setBlob(5, objectToBlob(agreeableness));
			ps.setBlob(6, objectToBlob(emotional_range));
			if(ps.executeUpdate() > 0){
				System.out.println("Personality Added");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
		
	}
	
	@SuppressWarnings("finally")
	public static boolean insertValues(String userId, Profile profile){
		boolean status = false;
		HashMap<String, Double> valuesMap = new HashMap<>();
		List<Trait> values = profile.getValues();
		for(Trait trait: values){
			Double percent = Double.parseDouble(String.format("%.2f",(trait.getPercentile() * 100.00)));
			valuesMap.put(trait.getName(), percent);
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.INSERT_VALUE);
			ps.setString(1, userId);
			ps.setBlob(2, objectToBlob(valuesMap));
			if(ps.executeUpdate() > 0){
				System.out.println("Value Added");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean insertNeeds(String userId, Profile profile){
		boolean status = false;
		HashMap<String, Double> needsMap = new HashMap<>();
		List<Trait> values = profile.getNeeds();
		for(Trait trait: values){
			Double percent = Double.parseDouble(String.format("%.2f",(trait.getPercentile() * 100.00)));
			needsMap.put(trait.getName(), percent);
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.INSERT_NEED);
			ps.setString(1, userId);
			ps.setBlob(2, objectToBlob(needsMap));
			if(ps.executeUpdate() > 0){
				System.out.println("Need Added");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	@SuppressWarnings({ "finally", "unchecked" })
	public static ArrayList<HashMap<String, Double>> getPersonality(String user_id){
		Connection con = null;
		ArrayList<HashMap<String, Double>> personality = new ArrayList<>(5);
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.SELECT_PERSONALITY);
			ps.setString(1, user_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Blob openness = rs.getBlob(1);
				Blob conscientiousness = rs.getBlob(2);
				Blob extraversion = rs.getBlob(3);
				Blob agreeableness = rs.getBlob(4);
				Blob emotional_range = rs.getBlob(5);
				personality.add((HashMap<String, Double>) blobToObject(openness.getBytes(1, (int)openness.length())));
				personality.add((HashMap<String, Double>) blobToObject(conscientiousness.getBytes(1, (int)conscientiousness.length())));
				personality.add((HashMap<String, Double>) blobToObject(extraversion.getBytes(1, (int)extraversion.length())));
				personality.add((HashMap<String, Double>) blobToObject(agreeableness.getBytes(1, (int)agreeableness.length())));
				personality.add((HashMap<String, Double>) blobToObject(emotional_range.getBytes(1, (int)emotional_range.length())));
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				return personality;
			}
		}
	}
	
	@SuppressWarnings({ "finally", "unchecked" })
	public static HashMap<String, Double> getNeed(String user_id){
		Connection con = null;
		HashMap<String, Double> need = null;
		
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.SELECT_NEED);
			ps.setString(1, user_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Blob result = rs.getBlob(1);
				need = (HashMap<String, Double>)blobToObject(result.getBytes(1, (int)result.length()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				return need;
			}
		}
	}
	
	
	@SuppressWarnings({ "unchecked", "finally" })
	public static HashMap<String, Double> getValue(String user_id){
		Connection con = null;
		HashMap<String, Double> value = null;
		
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.SELECT_VALUE);
			ps.setString(1, user_id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				Blob result = rs.getBlob(1);
				value = (HashMap<String, Double>)blobToObject(result.getBytes(1, (int)result.length()));
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				return value;
			}
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean updatePersonality(String user_id, Profile profile){
		boolean status = false;
		
		HashMap<String, Double> openness = new HashMap<>();
		HashMap<String, Double> conscientiousness = new HashMap<>();
		HashMap<String, Double> extraversion = new HashMap<>();
		HashMap<String, Double> agreeableness = new HashMap<>();
		HashMap<String, Double> emotional_range = new HashMap<>();
		
		List<Trait> personality = profile.getPersonality();
		
		for(Trait trait : personality){
			List<Trait> personalityChild = trait.getChildren();
			if(trait.getTraitId().equals("big5_openness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					openness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_conscientiousness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					conscientiousness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_extraversion")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					extraversion.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_agreeableness")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					agreeableness.put(traitChild.getName(), percent);
				}
			}
			else if(trait.getTraitId().equals("big5_neuroticism")){
				for(Trait traitChild: personalityChild){
					Double percent = Double.parseDouble(String.format("%.2f",(traitChild.getPercentile() * 100.00)));
					emotional_range.put(traitChild.getName(), percent);
				}
			}
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.UPDATE_PERSONALITY);
			ps.setBlob(1, objectToBlob(openness));
			ps.setBlob(2, objectToBlob(conscientiousness));
			ps.setBlob(3, objectToBlob(extraversion));
			ps.setBlob(4, objectToBlob(agreeableness));
			ps.setBlob(5, objectToBlob(emotional_range));
			ps.setString(6, user_id);
			if(ps.executeUpdate() > 0){
				System.out.println("Personality Updated");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				return status;
			}
		}
		
	}
	
	
	@SuppressWarnings("finally")
	public static boolean updateValues(String userId, Profile profile){
		boolean status = false;
		HashMap<String, Double> valuesMap = new HashMap<>();
		List<Trait> values = profile.getValues();
		for(Trait trait: values){
			Double percent = Double.parseDouble(String.format("%.2f",(trait.getPercentile() * 100.00)));
			valuesMap.put(trait.getName(), percent);
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.UPDATE_VALUE);
			ps.setBlob(1, objectToBlob(valuesMap));
			ps.setString(2, userId);
			if(ps.executeUpdate() > 0){
				System.out.println("Value Updated");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	
	@SuppressWarnings("finally")
	public static boolean updateNeeds(String userId, Profile profile){
		boolean status = false;
		HashMap<String, Double> needsMap = new HashMap<>();
		List<Trait> values = profile.getNeeds();
		for(Trait trait: values){
			Double percent = Double.parseDouble(String.format("%.2f",(trait.getPercentile() * 100.00)));
			needsMap.put(trait.getName(), percent);
		}
		
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.UPDATE_NEED);
			ps.setBlob(1, objectToBlob(needsMap));
			ps.setString(2, userId);
			if(ps.executeUpdate() > 0){
				System.out.println("Need Updated");
				status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
	
	@SuppressWarnings("finally")
	public static boolean UpdateUserPersonality(String userId, String date, Profile profile) {
		boolean status = false;
		Connection con = null;
		try{
			con = getConnection();
			PreparedStatement ps = con.prepareStatement(Constants.UPDATE_USER_PERSONALITY);
			ps.setString(1, date);
			ps.setString(2, userId);
			if(ps.executeUpdate() > 0){
				System.out.println("user_Personality Updated");
				if(updatePersonality(userId, profile) && updateValues(userId, profile) && updateNeeds(userId, profile))
					status = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				con.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			return status;
		}
	}
}
