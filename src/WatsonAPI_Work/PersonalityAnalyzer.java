package WatsonAPI_Work;

import java.io.StringReader;

import com.google.gson.stream.JsonReader;
import com.ibm.watson.developer_cloud.personality_insights.v3.PersonalityInsights;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Content;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.ProfileOptions;
import com.ibm.watson.developer_cloud.util.GsonSingleton;

public class PersonalityAnalyzer {
	private static final String username = "e27fd9db-f710-4994-b844-1fc48981310a";
	private static final String password = "P8eGBJgWlKhI";
	
	public static Profile personalityInsight(String userPost){
		
		PersonalityInsights service = new PersonalityInsights("2017-10-13");
		service.setUsernameAndPassword(username, password);
		Profile profile = null;
		try {
		  JsonReader jReader = new JsonReader(new StringReader(userPost));
		  Content content = GsonSingleton.getGson().fromJson(jReader, Content.class);
		  ProfileOptions options = new ProfileOptions.Builder()
		    .content(content).consumptionPreferences(true)
		    .rawScores(true).build();
		  profile = service.profile(options).execute();
		  //System.out.println(profile);
		} catch (Exception e) {
		  e.printStackTrace();
		}
		return profile; 
	}
}
