package utils;

public class Constants {
	public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver"; 
	public static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/Extractor";
	public static final String JDBC_USER = "root";
    public static final String JDBC_PASS = "mrinal2196";
    public static final String CREATE_TABLE_USER_PERSONALITY = "create table user_personality(user_id varchar(50) primary key, user_name varchar(100), perform_date date);";
    public static final String CREATE_TABLE_PERSONALITY = "create table personality(user_id varchar(50) primary key, big5_openness blob, big5_conscientiousness blob, big5_extraversion blob, big5_agreeableness blob, big5_neuroticism blob, foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_OPENNESS = "create table openness(user_id varchar(50) primary key, facet_adventurousness varchar(50), facet_artistic_interests varchar(50), facet_emotionality varchar(50), facet_imagination varchar(50), facet_intellect varchar(50), facet_liberalism varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_CONSCIENTIOUSNESS = "create table conscientiousness(user_id varchar(50) primary key, facet_achievement_striving varchar(50), facet_cautiousness varchar(50), facet_dutifulness varchar(50), facet_orderliness varchar(50), facet_self_discipline varchar(50), facet_self_efficacy varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_EXTRAVERSION = "create table extraversion(user_id varchar(50) primary key, facet_activity_level varchar(50), facet_assertiveness varchar(50), facet_cheerfulness varchar(50), facet_excitement_seeking varchar(50), facet_friendliness varchar(50), facet_gregariousness varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_AGREEABLENESS = "create table agreeableness(user_id varchar(50) primary key, facet_altruism varchar(50), facet_cooperation varchar(50), facet_modesty varchar(50), facet_morality varchar(50), facet_sympathy varchar(50), facet_trust varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_EMOTIONAL_RANGE = "create table emotional_range(user_id varchar(50) primary key, facet_anger varchar(50), facet_anxiety varchar(50), facet_depression varchar(50), facet_immoderation varchar(50), facet_self_consciousness varchar(50), facet_vulnerability varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_VALUE = "create table value(user_id varchar(50) primary key, value_conservation varchar(50), value_openness_to_change varchar(50), value_hedonism varchar(50), value_self_enhancement varchar(50), value_self_transcendence varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String CREATE_TABLE_NEEDS = "create table need(user_id varchar(50) primary key, need_challenge varchar(50), need_closeness varchar(50), need_curiosity varchar(50), need_excitement varchar(50), need_harmony varchar(50), need_ideal varchar(50), need_liberty varchar(50), need_love varchar(50), need_practicality varchar(50), need_self_expression varchar(50), need_stability varchar(50), need_structure varchar(50), foreign key(user_id) references user_personality(user_id));";
    public static final String INSERT_USER_PERSONALITY = "insert into user_personality values(?,?,?)";
    public static final String INSERT_PERSONALITY = "insert into personality values(?,?,?,?,?,?)";
    public static final String INSERT_VALUE = "insert into value values(?,?)";
    public static final String INSERT_NEED = "insert into need values(?,?)";
    public static final String IS_USER_ID_PRESENT = "select user_id from user_personality where user_id=?";
    public static final String SELECT_NEED = "select need from need where user_id=?";
    public static final String SELECT_PERSONALITY = "select big5_openness, big5_conscientiousness, big5_extraversion, big5_agreeableness, big5_neuroticism from personality where user_id=?";
    public static final String SELECT_VALUE = "select value from value where user_id=?";
    public static final String UPDATE_VALUE = "update value set value=? where user_id=?";
    public static final String UPDATE_NEED = "update need set need=? where user_id=?";
    public static final String UPDATE_PERSONALITY = "update personality set big5_openness=?, big5_conscientiousness=?, big5_extraversion=?, big5_agreeableness=?, big5_neuroticism=? where user_id=?";
    public static final String UPDATE_USER_PERSONALITY = "update user_personality set perform_date=? where user_id=?";
}
