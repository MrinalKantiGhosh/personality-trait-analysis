package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferences;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferencesCategory;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile;
import com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait;

import Database.DatabaseAccessObject;
import WatsonAPI_Work.*;

/**
 * Servlet implementation class GetAndSaveData_serv
 */
@WebServlet("/GetAndSaveData_serv")
public class GetAndSaveData_serv extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    
    public GetAndSaveData_serv() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");  
        PrintWriter out = response.getWriter();  
		
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		String userPost = request.getParameter("userPost");
		
		
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date());
		if(DatabaseAccessObject.isUserIdPresent(userId)){
			ArrayList<String> userInfo = new ArrayList<>();
			userInfo.add(userId);
			userInfo.add(userName);
			userInfo.add(date);
			userInfo.add(userPost);
			HttpSession session=request.getSession();    
			session.setAttribute("userInfo",userInfo);
			RequestDispatcher RequetsDispatcherObj = request.getRequestDispatcher("JSP/CheckNeedOfReanalyze.jsp");
			RequetsDispatcherObj.forward(request, response);
		}
		else{
			Profile profile = PersonalityAnalyzer.personalityInsight(userPost);
			if(DatabaseAccessObject.insertUserPersonality(userId, userName, date, profile)){
				request.setAttribute("profileData",profile);
				RequestDispatcher RequetsDispatcherObj =request.getRequestDispatcher("JSP/ShowPersonality.jsp");
				RequetsDispatcherObj.forward(request, response); 
			}else{
				out.println("Something went Wrong !!!!");
			}
		}
		
		
		
	}
	

}
