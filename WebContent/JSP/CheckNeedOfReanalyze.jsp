<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Extractor</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
</head>
<body>
	<%@ page import="java.util.*" %>
	<jsp:include page="../FBnav.html" />
	<%
		ArrayList<String> userInfo = (ArrayList<String>) session.getAttribute("userInfo");
		
		
	%>
	
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  		<div class="modal-dialog" role="document">
    		<div class="modal-content">
    		  <div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        		</button>
      		  </div>
      		  <div class="modal-body">
        		You have already done this Personality Insight Analyze. What you want to do now ?
      		  </div>
		      <div class="modal-footer">		          
	        	<a type="button" class="btn btn-primary" href="JSP/NewAnalyze.jsp">New Analyze</a>
		        <a type="button" class="btn btn-primary" href="JSP/ShowPreviousAnalyze.jsp">See Previous Analyze</a>
		      </div>
    		</div>
  		</div>
	</div>
	

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
	<script>
	$(window).on('load',function(){
        $('#myModal').modal({
        	backdrop:'static'
        });
    });
	
	</script>
</body>
</html>