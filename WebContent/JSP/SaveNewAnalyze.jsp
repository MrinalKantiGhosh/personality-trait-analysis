<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Extractor</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
</head>
<body>

	<!-- Declaration -->
	<%@ page import="WatsonAPI_Work.*" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferencesCategory" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferences" %>
	<%@ page import="java.util.*" %>
	<%@ page import="Database.*" %>

	<%
		if(session.getAttribute("userInfo") != null){
			ArrayList<String> userInfo = (ArrayList<String>)session.getAttribute("userInfo");
			String userId = userInfo.get(0);
			String userName = userInfo.get(1);
			String date = userInfo.get(2);
			String userPost = userInfo.get(3);
			Profile profile = PersonalityAnalyzer.personalityInsight(userPost);
			if(DatabaseAccessObject.UpdateUserPersonality(userId, date, profile)){
	%>
			<div id="alertSave">
				<div class="alert alert-success" id="success-alert">
    				<button type="button" class="close" data-dismiss="alert">x</button>
    				<strong>Success! </strong>
    				New personality analysis is added into databse.
				</div>
			</div>
			<jsp:include page="NewAnalyze.jsp" /> 
	<%
			 
			}
			
			
			
		}else{
			out.println("Session null SaveNewAnalyze");
		}
	%>
	
	
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

</body>
</html>