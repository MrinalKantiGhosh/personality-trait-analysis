<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Extactor</title>
	<link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,600i,700i" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	
	<style type="text/css">
			body{
				margin-top: 1%;
				background-color: #006064;
			}

			.card{
				margin-top: 1%;
			}
			
			.box{
				font-family: 'Crimson Text', serif;
				font-size: 18px;
			}
			
		</style>
</head>
<body>
	<!-- Declaration -->
	
	<%@ page import="Database.*" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferencesCategory" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferences" %>
	<%@ page import="java.util.*" %>
	
	
	
	
	<%
		if(session.getAttribute("userInfo") != null){
			ArrayList<String> userInfo = (ArrayList<String>)session.getAttribute("userInfo");
			String userId = userInfo.get(0);
			ArrayList<HashMap<String, Double>> personality = DatabaseAccessObject.getPersonality(userId);
			HashMap<String, Double> openness = personality.get(0);
			HashMap<String, Double> conscientiousness = personality.get(1);
			HashMap<String, Double> extraversion = personality.get(2);
			HashMap<String, Double> agreeableness = personality.get(3);
			HashMap<String, Double> emotional_range = personality.get(4);
			HashMap<String, Double> need = DatabaseAccessObject.getNeed(userId);
			HashMap<String, Double> value = DatabaseAccessObject.getValue(userId);
				
			
	%>	
			<div class="container">
			<a class="btn btn-success offset-md-1 col col-md-3" href="../FBnav.html">Home</a>
			<div id="accordion">
				<div class="card">
					<div class="card-header" id="personality">
						<h2 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapsePersonality" aria-expanded="false" aria-controls="collapsePersonality">
						Personality
						</button>
						</h2>
					</div>
					<div id="collapsePersonality" class="collapse" aria-labelledby="personality" data-parent="#accordion">
						<div class="card-body">
							<div class="row justify-content-around">
							
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4>Openness</h4></div>
										<div class="card-body box">
										<%
											for(String key:openness.keySet()){
												Double percent = (Double)openness.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "><%= key %> = <%= percent %></div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>																	
											
										</div>
									</div>
									
								
									
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4>Conscientiousness</h4></div>
										<div class="card-body box">
										<%
											for(String key:conscientiousness.keySet()){
												Double percent = (Double)conscientiousness.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>																	
											
										</div>
									</div>
									
									
									
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4>Extraversion</h4></div>
										<div class="card-body box">
										<%
											for(String key:extraversion.keySet()){
												Double percent = (Double)extraversion.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>																	
											
										</div>
									</div>
									
									
									
									
									
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4>Agreeableness</h4></div>
										<div class="card-body box">
										<%
											for(String key:agreeableness.keySet()){
												Double percent = (Double)agreeableness.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>																	
											
										</div>
									</div>
									
									
									
									
									
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4>emotional_range</h4></div>
										<div class="card-body box">
										<%
											for(String key:emotional_range.keySet()){
												Double percent = (Double)emotional_range.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>																	
											
										</div>
									</div>
								
							
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="card">
					<div class="card-header" id="needs">
						<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNeeds" aria-expanded="false" aria-controls="collapseNeeds">
						Needs
						</button>
						</h5>
					</div>
					<div id="collapseNeeds" class="collapse" aria-labelledby="needs" data-parent="#accordion">
						<div class="card-body">
						
							<div class="row justify-content-around">
								<div class="card col col-md-5 col-sm-10">
									<div class="card-header text-center"><h4>Needs</h4></div>
									<div class="card-body box">
										
										<%
											for(String key:need.keySet()){
												Double percent = (Double)need.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>	
												
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
				
				<div class="card">
					<div class="card-header" id="values">
						<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseValues" aria-expanded="false" aria-controls="collapseValues">
						Values
						</button>
						</h5>
					</div>
					<div id="collapseValues" class="collapse" aria-labelledby="values" data-parent="#accordion">
						<div class="card-body">
							
							<div class="row justify-content-around">
								<div class="card col col-md-5 col-sm-10">
									<div class="card-header text-center"><h4>Values</h4></div>
									<div class="card-body box">
										
										<%
											for(String key:value.keySet()){
												Double percent = (Double)value.get(key);
												if(percent < 25.00){
										%>
											<div class="alert alert-danger "> <%= key %> = <%= percent %> </div>
										<%													
												}else if(percent >= 25.00 && percent < 50.00){
										%>
											<div class="alert alert-warning "> <%= key %> = <%= percent %> </div>
										<%	
												}else if(percent >= 50.00 && percent < 75.00){
										%>
											<div class="alert alert-primary "> <%= key %> = <%= percent %> </div>
										<%
												}else{
										%>
											<div class="alert alert-success "> <%= key %> = <%= percent %> </div>
										<%
												}
											}
										%>	
												
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				
				
				
			</div>
		</div>
	<% 
		}else{
			out.println("Session null ShowPreviousAnalyze");
		}
		
	%>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
	<script src="js/app.js"></script>
</body>
</html>