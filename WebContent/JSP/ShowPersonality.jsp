<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Personality Insights</title>
<!-- <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,600i,700i" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous"> -->
<style type="text/css">
			body{
				margin-top: 1%;
				background-color: #006064;
			}

			.card{
				margin-top: 1%;
			}
			
			.box{
				font-family: 'Crimson Text', serif;
				font-size: 18px;
			}
			
		</style>
</head>
<body>
	<!-- Declaration -->
	
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Profile" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.Trait" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferencesCategory" %>
	<%@ page import="com.ibm.watson.developer_cloud.personality_insights.v3.model.ConsumptionPreferences" %>
	<%@ page import="java.util.*" %>
	
	<%@include  file="../FBnav.html" %>
	<script type="text/javascript">
	$("facebookLogOut").click(function() {
		  $("#showPersonality").remove();
		});
	</script>
	
	<div id="showPersonality">
		<%
		Profile profile = (Profile)request.getAttribute("profileData");
		List<Trait> personality = profile.getPersonality();
		List<Trait> values = profile.getValues();
		List<Trait> needs = profile.getNeeds();
		List<ConsumptionPreferencesCategory> consumptionPreferenceCategory = profile.getConsumptionPreferences();
	%>
	
	<div class="container">
			<div id="accordion">
				<div class="card">
					<div class="card-header" id="personality">
						<h2 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapsePersonality" aria-expanded="false" aria-controls="collapsePersonality">
						Personality
						</button>
						</h2>
					</div>
					<div id="collapsePersonality" class="collapse" aria-labelledby="personality" data-parent="#accordion">
						<div class="card-body">
							<div class="row justify-content-around">
							<%
								for(Trait trait:personality){
							%>
									<div class="card col col-md-5 col-sm-10">
										<div class="card-header text-center"><h4><%=trait.getName() %></h4></div>
										<div class="card-body box">
										<%
											List<Trait> personalityChild = trait.getChildren();
											for(Trait traitChild: personalityChild){
												String percent = String.format("%.2f",(traitChild.getPercentile() * 100.00));
												if(Double.parseDouble(percent) < 25.00){
												%>
													<div class="alert alert-danger "><%=traitChild.getName() %> = <%=percent %></div>
												<%	
												}else if(Double.parseDouble(percent) >= 25.00 && Double.parseDouble(percent) < 50.00){
												%>
													<div class="alert alert-warning "><%=traitChild.getName() %> = <%=percent %></div>
												<%
												}else if(Double.parseDouble(percent) >= 50.00 && Double.parseDouble(percent) < 75.00){
												%>
													<div class="alert alert-primary "><%=traitChild.getName() %> = <%=percent %></div>
												<%					
												}else{
												%>
													<div class="alert alert-success "><%=traitChild.getName() %> = <%=percent %></div>
												<%					
												}
											}
										%>
											
										</div>
									</div>
								
							<% 
								}
							%>
							</div>
						</div>
					</div>
				</div>
				
				
				
				<div class="card">
					<div class="card-header" id="needs">
						<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseNeeds" aria-expanded="false" aria-controls="collapseNeeds">
						Needs
						</button>
						</h5>
					</div>
					<div id="collapseNeeds" class="collapse" aria-labelledby="needs" data-parent="#accordion">
						<div class="card-body">
						
							<div class="row justify-content-around">
								<div class="card col col-md-5 col-sm-10">
									<div class="card-header text-center"><h4>Needs</h4></div>
									<div class="card-body box">
										<%
											for(Trait trait: needs){
												String percent = String.format("%.2f",(trait.getPercentile() * 100.00));
												if(Double.parseDouble(percent) < 25.00){
												%>
													<div class="alert alert-danger"><%=trait.getName() %> = <%=percent %></div>
												<%
												}else if(Double.parseDouble(percent) >= 25.00 && Double.parseDouble(percent) < 50.00){
												%>
													<div class="alert alert-warning"><%=trait.getName() %> = <%=percent %></div>
												<%	
												}else if(Double.parseDouble(percent) >= 50.00 && Double.parseDouble(percent) < 75.00){
												%>
													<div class="alert alert-primary"><%=trait.getName() %> = <%=percent %></div>
												<%	
												}else{
												%>
													<div class="alert alert-success"><%=trait.getName() %> = <%=percent %></div>
												<%	
												} 	
											}
										%>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
				
				
				<div class="card">
					<div class="card-header" id="values">
						<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseValues" aria-expanded="false" aria-controls="collapseValues">
						Values
						</button>
						</h5>
					</div>
					<div id="collapseValues" class="collapse" aria-labelledby="values" data-parent="#accordion">
						<div class="card-body">
							
							<div class="row justify-content-around">
								<div class="card col col-md-5 col-sm-10">
									<div class="card-header text-center"><h4>Values</h4></div>
									<div class="card-body box">
										<%
											for(Trait trait: values){
												String percent = String.format("%.2f",(trait.getPercentile() * 100.00));
												if(Double.parseDouble(percent) < 25.00){
												%>
													<div class="alert alert-danger"><%=trait.getName() %> = <%=percent %></div>
												<%	
												}else if(Double.parseDouble(percent) >= 25.00 && Double.parseDouble(percent) < 50.00){
												%>
													<div class="alert alert-warning"><%=trait.getName() %> = <%=percent %></div>
												<%	
												}else if(Double.parseDouble(percent) >= 25.00 && Double.parseDouble(percent) < 50.00){
												%>
													<div class="alert alert-primary"><%=trait.getName() %> = <%=percent %></div>
												<%	
												}else{
												%>
													<div class="alert alert-success"><%=trait.getName() %> = <%=percent %></div>
												<%	
												} 	
											}
										%>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="card">
					<div class="card-header" id="consumption_preferences">
						<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapseConsumption_preferences" aria-expanded="false" aria-controls="collapseConsumption_preferences">
						Consumption Preferences
						</button>
						</h5>
					</div>
					<div id="collapseConsumption_preferences" class="collapse" aria-labelledby="consumption_preferences" data-parent="#accordion">
						<div class="card-body">
							<div class="row justify-content-around">
								
							<%
								for(ConsumptionPreferencesCategory consumption_preferences_category : consumptionPreferenceCategory){
							%>
							
								<div class="card col col-md-5 col-sm-10">
									<div class="card-header text-center"><h4><%= consumption_preferences_category.getName() %></h4></div>
									<div class="card-body box">
									<%
										List<ConsumptionPreferences> consumption_preference = consumption_preferences_category.getConsumptionPreferences();
										for(ConsumptionPreferences consumptionPrefernce: consumption_preference){
											double score = consumptionPrefernce.getScore();
											if(score < 0.5){
											%>
												<div class="alert alert-danger"><%=consumptionPrefernce.getName() %> = <%=score %></div>
											<%	
											}else if(score >= 0.5 && score < 1.0){
											%>
												<div class="alert alert-primary"><%=consumptionPrefernce.getName() %> = <%=score %></div>
											<%	
											}else{
											%>
												<div class="alert alert-success"><%=consumptionPrefernce.getName() %> = <%=score %></div>
											<%	
											}
										}
									%>
									</div>
								</div>
							<%	
								}
							%>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
	</div>
	
	
	
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> -->
</body>
</html>