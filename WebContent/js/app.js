// Facebook

var accessToken;
var URL;
var userId;
var messageArray = [];
var messageJSON = {
  "contentItems":[]
};

function statusChangeCallback(response) {
  if(response.status === 'connected') {
    accessToken = response.authResponse.accessToken;
    document.getElementById('logInStatus').textContent = " You are logged into Facebook, and has logged into your app.";
    checkisLoggedIn(true);
  } else if(response.status === 'not_authorized') {
    document.getElementById('logInStatus').textContent = "You are logged into Facebook, but has not logged into your app.";
    checkisLoggedIn(false);
  } else {
    document.getElementById('logInStatus').textContent = "You are not logged into Facebook, it cannot connect to Facebook.";
    checkisLoggedIn(false);
  }
}


function logInWithFacebook() {
  FB.login(function(response) {
    statusChangeCallback(response);
    getFeed();
    isLoggedIn = true;
    checkisLoggedIn(isLoggedIn);
  });
}

function logOutFromFacebook() {
  FB.logout(function(response) {
    statusChangeCallback(response);
    isLoggedIn = false;
    checkisLoggedIn(isLoggedIn);
  });
}

function checkisLoggedIn(isLoggedInValue) {
  if(!isLoggedInValue) {
	  //window.location = "AfterLogIn.jsp";
    document.getElementById('facebookLogIn').style.display = "block";
    document.getElementById('facebookLogOut').style.display = "none";
    document.getElementById('getPersonality').style.display = "none";
  } else {
	  //window.location = "index.html";
    document.getElementById('facebookLogIn').style.display = "none";
    document.getElementById('facebookLogOut').style.display = "block";
    //document.getElementById('getPersonality').style.display = "block";
  }
}




//------------------------------------------------------------------------------------------------------------------
$(document).ready(function(){
	 
});


function getFeed(){
    FB.api(
      '/me',
      'GET',
      {"fields":"about,birthday,picture{url},email,name,id"},
      function(response) {
        formValueIdName(response.id, response.name);
        var URL = "https://graph.facebook.com/" + response.id + "?fields=feed.limit(1000)&access_token="+accessToken;
        //console.log("first url from  = " + URL);
        getJsonResult(URL);
      }
  );
}

var postCount = 0;
var isFirst = true;
function getJsonResult(URL){

  $.getJSON(URL, function(result){
    postCount += printPost(result);
    try{
      if(isFirst && result.feed.paging.next != null){
         getJsonResult(result.feed.paging.next);
         isFirst = false;
       }
      else if(result.paging.next != undefined || result.paging != undefined || result != undefined)
        getJsonResult(result.paging.next);
    }catch(err){
        console.log("finish");
        console.log(messageJSON);
        document.getElementById('getPersonality').style.display="block";
        var post = JSON.stringify(messageJSON);
        formValuePost(post);
    }
    //console.log("post count = " + postCount);

  });
}


/*function printPost(result){
  //console.log("post count = " + postCount);
  if(postCount == 0)
    response = result.feed;
  else
    response = result;
  var count = 0

  for(var post = 0; post < response.data.length; post++){
    var posts = document.createElement("div");
    if(response.data[post].message != null){
      posts.innerHTML = "<hr><h3>Id = "+ response.data[post].id +"<br>Date = " + response.data[post].created_time.substring(0,10) + "<br>Message = " + response.data[post].message + "</h3><hr>";
    }else {
        posts.innerHTML = "<hr><h3>Id = "+ response.data[post].id +"<br>Date = " + response.data[post].created_time.substring(0,10) + "</h3><hr>";
    }
    document.getElementById('showPost').appendChild(posts);
    count++;
  }
  return count;
}*/




function printPost(result){
  //console.log("post count = " + postCount);
  if(postCount == 0)
    response = result.feed;
  else
    response = result;
  var count = 0

  for(var post = 0; post < response.data.length; post++){
    //var posts = document.createElement("div");
    if(response.data[post].message != null){
      messageArray.push(response.data[post].message);
      messageJSON.contentItems.push({
        "content": response.data[post].message
      });
      //posts.innerHTML = "<hr><h3>Id = "+ response.data[post].id +"<br>Date = " + response.data[post].created_time.substring(0,10) + "<br>Message = " + response.data[post].message + "</h3><hr>";
    }
    //document.getElementById('showPost').appendChild(posts);
    count++;
  }
  return count;
}




//***********************************************


function formValueIdName(id, name){
  document.getElementById('userId').value = id;
  document.getElementById('userName').value = name;
}

function formValuePost(post){
  document.getElementById('userPost').value = post;
}
